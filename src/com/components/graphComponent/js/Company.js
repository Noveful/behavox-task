/**
 * Created by Anna Solodovnikova on 03.08.2015.
 */

function Company(data, layer){
    this.title = data.title;
    this.departments = [];
    this.rays = [];

    this.cx = Math.round(window.innerWidth / 2);
    this.cy = Math.round(window.innerHeight / 2);
    this.r = 50;
    this.rayLength = Math.min(window.innerHeight, window.innerWidth)/3;

    var areDepartmentsShown = false;
    var elementsPadding = 5;

    var fadeDuration = 500,
        breakDuration = 500;

    this.draw = function() {
        var self = this;

        layer.append("circle")
            .attr("cx", this.cx)
            .attr("cy", this.cy)
            .attr("r", this.r)
            .attr("fill", "orange")
            .style("cursor", "pointer")
            .on("click", function(){
                if (areDepartmentsShown) return;
                self.setDepartments(data.departments);
                self.drawSatellites();
                areDepartmentsShown = true;
            });

        //add title to the company circle
        layer.append("text")
            .attr("x", this.cx - this.r)
            .attr("y", this.cy - this.r - elementsPadding)
            .text(this.title)
            .attr("fill", "black")
            .attr("font-size", "25px");
    };

    this.setDepartments = function(data){
        var departmentCx = 0;
        var departmentCy = 0;
        var angle = 2*Math.PI/data.length;

        for (var row in data){
            departmentCx = Math.round(this.cx+this.rayLength*Math.cos(angle*row-Math.PI));
            departmentCy = Math.round(this.cy+this.rayLength*Math.sin(angle*row-Math.PI));

            this.rays.push(new Ray(this.cx, this.cy, departmentCx, departmentCy));
            this.departments.push(new Department(data[row], departmentCx, departmentCy, layer));
        }
    };

    this.drawSatellites = function(){
        this.drawLines();
        this.drawDepartments();
    }

    this.drawLines = function(){
        var lineClass = "department_ray";
        var i = 0;

        layer
            .selectAll("." + lineClass)
            .data(this.rays)
            .enter()
            .append("line")
            .attr("x1",function(ray){return ray.x1;})
            .attr("y1", function(ray){return ray.y1;})
            .attr("x2", function(ray){return ray.x2;})
            .attr("y2", function(ray){return ray.y2;})
            .attr("class", lineClass)
            .style("stroke", "orange")
            .style("stroke-width", 2)
            .style("opacity", 0)
            .transition()
            .duration(fadeDuration)
            .delay(function(){return breakDuration*i++;})
            .style("opacity", 1);
    }

    this.drawDepartments = function(){
        var circleClass = "department_circle";
        var labelClass = "department_title";
        var self = this;
        var i = 0;

        layer
            .selectAll("."+circleClass)
            .data(this.departments)
            .enter()
            .append("circle")
            .attr("cx",function(dep){return dep.cx;})
            .attr("cy",function(dep){return dep.cy;})
            .attr("r", function(dep){return dep.r;})
            .attr("fill","#8dcbf2")
            .attr("class", circleClass)
            .on("click", function(dep){dep.zoom(10, self.cx, self.cy)})
            .style("cursor", "pointer")
            .style("opacity", 0)
            .transition()
            .duration(fadeDuration)
            .delay(function(){return breakDuration*i++})
            .style("opacity", 1);

        i=0;
        layer
            .selectAll("."+labelClass)
            .data(this.departments)
            .enter()
            .append("text")
            .attr("x", function(dep){return (dep.cx-dep.r);})
            .attr("y", function(dep){return (dep.cy-dep.r-elementsPadding);})
            .attr("class", labelClass)
            .text(function(dep){return dep.title;})
            .attr("fill", "black")
            .style("opacity", 0)
            .transition()
            .duration(fadeDuration)
            .delay(function(){return breakDuration*i++;})
            .style("opacity", 1);

    }

    function Ray(x1, y1, x2, y2){
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }
}