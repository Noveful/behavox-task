/**
 * Created by Anna Solodovnikova on 03.08.2015.
 */

function Department(data, cx, cy, layer){
    this.id = data.id;
    this.title = data.title;
    this.membersCount = data.members;

    this.cx = cx;
    this.cy = cy;
    this.r = 15;
    this.members = [];

    this.isNodeScaled = false;   //is department zoomed

    var centeredLabel;
    var elementsPadding = 5,
        scrollbarWidth = 20;
    var memberR = 1,
        memberCircleClass = "member_circle";

    var fadeDuration = 500;

    this.zoom = function(scaleExtent, centerBodyCx, centerBodyCy){
        if (this.isNodeScaled){
            this.zoomOut();
            this.isNodeScaled=false;
            return;
        }

        this.zoomIn(scaleExtent, centerBodyCx, centerBodyCy);
        this.isNodeScaled=true;
    };

    this.zoomOut = function(){
        layer.selectAll(".department_title")
            .transition()
            .duration(fadeDuration)
            .style("opacity", "1");

        centeredLabel
            .transition()
            .duration(fadeDuration)
            .style("opacity", 0)
            .remove();

        layer
            .selectAll("."+memberCircleClass)
            .transition()
            .duration(fadeDuration)
            .style("opacity", 0)
            .remove();

        layer
            .transition()
            .duration(fadeDuration)
            .attr("transform", "scale(1)");
    };

    this.zoomIn = function(scaleExtent, centerBodyCx, centerBodyCy){
        //create label inside department circle
        centeredLabel = layer
            .append("text")
            .attr("x", this.cx-this.r+elementsPadding)
            .attr("y", this.cy)
            .text(this.title)
            .style("font-size", "3px");

        this.shiftLayer(scaleExtent, centerBodyCx, centerBodyCy);
        this.setMembers(scaleExtent);
        this.drawMembers();

        layer.selectAll(".department_title")
            .transition()
            .duration(fadeDuration)
            .style("opacity", "0");
    };

    this.shiftLayer = function(scaleExtent, centerBodyCx, centerBodyCy){
        //shifts for moving the visible part of viewport
        var shiftx = centerBodyCx - this.cx*scaleExtent,
            shifty = centerBodyCy - this.cy*scaleExtent;

        //zoom front layer and move the visible part of viewport
        layer
            .transition()
            .duration(fadeDuration)
            .attr("transform", "translate("+shiftx+","+shifty+")scale("+scaleExtent+")");
    };

    this.setMembers = function(scaleExtent){
        this.members = [];

        //angle between member circles for even distribution around department circle
        var angle = 2*Math.PI/this.membersCount;

        //the range to change radius of imaginary circle that is between department circle and viewport edge
        var range =
            Math.min(window.innerHeight, window.innerWidth)/2 -
            this.r*scaleExtent - elementsPadding*scaleExtent - scrollbarWidth - memberR*scaleExtent ;

        for (var i = 0;i < this.membersCount;i++){
            //get radius of the imaginary circle
            var imagineR = Math.random()*range/scaleExtent+this.r+elementsPadding;
            //get coordinates on the edge of the imaginary circle
            var x = Math.round(this.cx+imagineR*Math.cos(angle*i-Math.PI)),
                y = Math.round(this.cy+imagineR*Math.sin(angle*i-Math.PI));

            var memberData = {
                "name": "Peter Fedorov",
                "department": this.title,
                "phone": "+79297536142",
                "email": "p.fedorov@gmail.com"
            };

            this.members.push(new Member(memberData, x, y, memberR));
        }
    };

    this.drawMembers = function(){
        layer
            .selectAll("." + memberCircleClass).data(this.members)
            .enter()
            .append("circle")
            .attr("cx", function(member){return member.cx;})
            .attr("cy", function(member){return member.cy;})
            .attr("r", function(member){return member.r;})
            .attr("fill", "#94cd8e")
            .attr("class", memberCircleClass)
            .on("mouseover", function(member){
                member.showTooltip();
            })
            .on("mouseout", function(member){
                member.hideTooltip();
            });
    };
}