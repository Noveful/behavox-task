/**
 * Created by Anna Solodovnikova on 27.07.2015.
 */
window.com_components_graphComponent_GraphComponent =
    function() {
        //get data from state
        var dataset = this.getState().company;

        var scrollbarWidth = 20;
        var zoomListener = d3.behavior.zoom();

        var svg = d3.select(this.getElement())
            .append("svg")
            .attr("width", window.innerWidth - scrollbarWidth)
            .attr("height", window.innerHeight);

        var frontLayer = svg.append("g")
            .attr("class", "front_layer");
        frontLayer.call(zoomListener);

        var company = new Company(dataset, frontLayer);
        company.draw();


    };