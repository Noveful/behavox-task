/**
 * Created by Anna Solodovnikova on 04.08.2015.
 */

function Member(data, cx, cy, r){
    this.name = data.name;
    this.department = data.department;
    this.phone = data.phone;
    this.email = data.email;

    this.cx = cx;
    this.cy = cy;
    this.r = r;

    var tip = d3.select("body")
        .append("div")
        .style("opacity", 0)
        .style("position", "absolute")
        .style("z-index", 10)
        .attr("stroke", "black");

    this.hideTooltip = function(){
        tip
            .style("opacity", 0)
            .style("display", "none");
    }

    this.showTooltip = function(){
        tip.html(this.getTipHtml(this.name, this.department, this.phone, this.email));

        var coordinates = this.getTipCoordinates();
        tip.style("left", coordinates[0] + "px")
            .style("top", coordinates[1] + "px")
            .style("opacity", 1)
            .style("display", "block");
    };

    this.getTipCoordinates = function(){
        var tipRemote = 30,
            mouseX = d3.event.pageX,
            mouseY = d3.event.pageY;

        var leftX = mouseX + tipRemote,
            topY = mouseY - tipRemote;

        //case of when mouse is close to edge
        var tipElement = document.getElementById("member_tooltip"),
            tipWidth = tipElement.offsetWidth,
            tipHeight = tipElement.offsetHeight;

        if (window.innerWidth-mouseX < tipWidth)
            leftX = mouseX - tipWidth - tipRemote;
        if (window.innerHeight-mouseY<tipHeight)
            topY = mouseY - tipHeight - tipRemote;

        return [leftX, topY];
    }

    this.getTipHtml = function(name, department, phone, email){
        return '<div id="member_tooltip" style="' +
            'padding:10px;' +
            'background-color:#d1e7f0;' +
            'border:1px solid #91afba;' +
            'border-radius:10px;' +
            'text-align:center;"' +
            '<strong>Name:</strong> ' + name + '</br>' +
            '<strong>Department: </strong>' + department + '</br>' +
            '<strong>Phone number:</strong> ' + phone + '</br>' +
            '<strong>Email:</strong> ' + email + '</br>' +
            '</div>';
    };
}