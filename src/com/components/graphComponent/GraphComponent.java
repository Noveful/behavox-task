package com.components.graphComponent;

import com.models.Company;
import com.vaadin.annotations.JavaScript;
import com.vaadin.ui.AbstractJavaScriptComponent;

/**
 * Created by Anna Solodovnikova on 27.07.2015.
 */
@JavaScript({"js/d3.min.js", "js/company.js", "js/department.js", "js/member.js", "js/graph_component.js"})
public class GraphComponent extends AbstractJavaScriptComponent {

    public void setCompany(Company company){
        getState().company = company;
    }

    public Company getCompany(){
        return getState().company;
    }

    @Override
    protected GraphComponentState getState(){
        return (GraphComponentState) super.getState();
    }
}
