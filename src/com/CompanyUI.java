package com;

import com.components.graphComponent.GraphComponent;
import com.models.Company;
import com.models.Department;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anna Solodovnikova on 27.07.2015.
 */
public class CompanyUI extends UI {
    @Override
    public void init(VaadinRequest request) {
        List<Department> departments = new ArrayList<Department>();
        departments.add(new Department(new Long(1), "Programmers", 100));
        departments.add(new Department(new Long(2), "Analysts", 125));
        departments.add(new Department(new Long(3), "Researchers", 135));
        departments.add(new Department(new Long(4), "Writers", 100));
        departments.add(new Department(new Long(5), "Managers", 145));

        Company company = new Company("Behavox", departments);

        GraphComponent graphComponent = new GraphComponent();
        graphComponent.setCompany(company);
        setContent(graphComponent);
    }
}
