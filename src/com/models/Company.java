package com.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anna Solodovnikova on 30.07.2015.
 */
public class Company {
    private String title;
    private List<Department> departments;

    public Company(String title, List<Department> departments){
        this.title = title;
        this.departments = new ArrayList<Department>();
        for (Department department: departments){
            this.departments.add(department);
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }
}
