package com.models;

/**
 * Created by Anna Solodovnikova on 30.07.2015.
 */
public class Department {
    private Long id;
    private String title;
    private int members;

    public Department(Long id, String title, int members){
        this.id = id;
        this.title = title;
        this.members = members;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMembers() {
        return members;
    }

    public void setMembers(int members) {
        this.members = members;
    }
}
